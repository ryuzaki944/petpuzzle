import React, { useState, useEffect } from "react";
import Timer from "react-timer-wrapper";
import Timecode from "react-timecode";
import Table from "./Table";


function CountUp(props) {
    const [timerStart, settimerStart] = useState(false);
    const [time, setTime] = useState();

    const startTimer = () => {
        settimerStart(true);
    };
    const stopTimer = () => {
        settimerStart(false);
    };
    useEffect(() => {
        if(props.value === 'start'){
            settimerStart(true);
        }else if (props.value === 'stop'){
            settimerStart(false);
            // setTime()
        }
    }, [props])

    const onTimerStop = ({duration, progress, time}) => {
        console.log('Duration: ', duration);    
        console.log('Progress: ', progress);    
        console.log('Time: ', Math.floor(time / 1000)); 
        setTime(Math.floor(time / 1000))
        settimerStart(false)   
    }

    return (
        <div className="countup">
            <div className="countup-inner">
                <Timer active={timerStart} duration={1000 * 60 * 3} onStop={onTimerStop}>
                    <Timecode />
                </Timer>
                <button onClick={startTimer}>Start Timer</button>
            </div>
            <Table value={time} />
        </div>
    );
}

export default CountUp;
