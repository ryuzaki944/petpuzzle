import React from "react";
import Generator from "./Generator";

function Dashboard() {
    return (
        <div className="container">
            <Generator />
        </div>
    );
}

export default Dashboard;
