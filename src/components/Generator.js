import React from 'react'
import Cube from './Cube';

function Generator() {

    let arr = []

    const ranNum = () => {
        let temp = []
        while(temp.length < 18){
            var r = Math.floor(Math.random() * 18) + 1;
            if(temp.indexOf(r) === -1) temp.push(r);
        }
        arr = [...arr, ...temp]
    }
    ranNum()
    ranNum()

    return (
        <div>
            <Cube value={arr} />
        </div>
    )
}

export default Generator
