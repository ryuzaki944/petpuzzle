import React, { useState, useEffect } from "react";
import CountUp from "./Timer";

let timeout;
let totalOpened = 0;

function Cube(props) {
    const [openedCards, setOpenedCards] = useState([]);
    const [begin, setBegin] = useState();

    useEffect(() => {
        Spinner();
        Remover();
    }, [openedCards]);

    const onClick = (data) => {
        setBegin('start');
        if (openedCards.length > 1) {
            clearTimeout(timeout);
            return;
        }
        let parent = data.target.parentElement;
        let hidden = data.target;
        let number = data.target.nextElementSibling;
        hidden.style.transform = "rotate3d(0, 1, 0, 180deg)";
        hidden.style.zIndex = "0";
        number.style.transform = "rotate3d(0, 1, 0, 0deg)";
        number.style.zIndex = "1";

        if (openedCards.length === 1) {
            clearTimeout(timeout);
            setOpenedCards((openedCards) => [...openedCards, parent]);
        } else if (openedCards.length === 0) {
            setOpenedCards([parent]);
            timeout = setTimeout(() => {
                hidden.style.transform = "rotate3d(0, 1, 0, 0deg)";
                hidden.style.zIndex = "1";
                number.style.transform = "rotate3d(0, 1, 0, 180deg)";
                number.style.zIndex = "0";
            }, 5000);
        }
    };

    const Card = (num, index) => (
        <div id={index} key={index} className="card">
            <div className="card__hidden" onClick={(e) => onClick(e)}></div>{" "}
            <h2>{num}</h2>
        </div>
    );

    const CardMap = props.value?.map((item, i) => Card(item, i));

    const Spinner = () => {
        if (openedCards.length === 0) return;
        const time = openedCards.length == 1 ? 5000 : 500;
        setTimeout(() => {
            openedCards.forEach((card) => {
                card.firstChild.style.transform = "rotate3d(0, 1, 0, 0deg)";
                card.firstChild.style.zIndex = "1";
                card.lastChild.style.transform = "rotate3d(0, 1, 0, 180deg)";
                card.lastChild.style.zIndex = "0";
            });
            setOpenedCards([]);
        }, time);
    };

    const Remover = () => {
        if (CardMap) {
            if (openedCards.length === 2) {
                let firstCard = openedCards[0];
                let secondCard = openedCards[1];

                if (
                    firstCard.lastChild.innerHTML === secondCard.lastChild.innerHTML
                ) {
                    firstCard.classList.add("remove");
                    secondCard.classList.add("remove");
                    // console.log(++totalOpened)
                    if (++totalOpened == 18) {
                        setBegin('stop')
                        console.log('stop')
                    }
                }
            }
        }
    };

    return (
        <div className="cube">
            {CardMap}
            <CountUp value={begin} />
        </div>
    );
}

export default Cube;
